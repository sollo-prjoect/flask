from flask import Flask, render_template, request, flash, redirect, url_for, session

app = Flask(__name__)
app.secret_key = '1234567890'

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/reg', methods=['GET', 'POST'])
def reg():
    if 'logined' in session:
        return redirect(url_for('user', name=session['logined']))

    if request.method == 'POST':
        print(request.form)
        print(request.form['username'])
        if len(request.form['username']) > 0:
            session['logined'] = request.form["username"]
            return redirect(url_for('user', name=session['logined']))
        else:
            flash("ЭЭЭЭЭЭЭЭЭЭЭЭЭЭЭЭЭЭЭ введи ты имя, не жопся", category='error')
    return render_template('auth.html')

@app.route('/user/<name>')
def user(name):
    return render_template('user.html', username=name)

@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404

if __name__ == "__main__":
    app.run(debug=True)